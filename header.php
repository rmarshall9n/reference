<?php

require_once "app/DirectoryManager.php";
require_once "app/RefParser.php";

$dir = new DirectoryManager();

if (isset($_POST['create'])) {
    if($dir->create($_POST['create'])) {
        $_GET['r'] = $_POST['create'];
    }
}

$dir->getDirectoryListing();

$refName = empty($_GET['r']) ? 'default' : $_GET['r'];

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="/js/jquery/jquery-3.1.1.min.js"></script>

    <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap/bootstrap-theme.min.css">
    <script src="/js/bootstrap/bootstrap.min.js"></script>

    <link rel="stylesheet" href="/css/highlight/monokai.css">
    <script src="/js/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <style type="text/css">
        pre {border: 0; background-color: transparent;}
        pre code {border-radius: 5px;}
        .title:after {content:' '; display:block; border:1px solid black;}
        body {padding-top: 70px;}
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">

            <div class="navbar-left">
                <p class="navbar-text"><a href="index.php" class="navbar-link">Reference</a></p>
                <a href="index.php?r=<?= $refName; ?>" class="btn btn-default navbar-btn">View</a>
                <a href="edit.php?r=<?= $refName; ?>" class="btn btn-default navbar-btn">Edit</a>
            </div>

            <form method="post" class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" name="create" class="form-control" placeholder="File Name">
                </div>
                <button type="submit" class="btn btn-default">Create</button>
            </form>
        </div>
      </div>
    </nav>