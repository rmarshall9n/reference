<?php

require_once 'header.php';
$html = '';
if($dir->exists($refName)) {
    $html = $dir->getRefContent($refName);
    $html = (new RefParser)->parse($html);
}

?>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-md-2">
            <h1></h1>
            <ul id="thisMenu" class="nav nav-pills nav-stacked">
                <?php foreach ($dir->getListing() as $name => $path) : ?>
                    <li><a href="?r=<?= $name; ?>"><?= $name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-10">
            <?= $html; ?>
        </div>
    </div>
</div>


<?php require_once 'footer.php'; ?>