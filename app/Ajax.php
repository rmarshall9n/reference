<?php


/**
* Ajax router
*/
class Ajax
{

    function __construct() {}

    public function save($data)
    {
        require_once "DirectoryManager.php";
        $dir = new DirectoryManager();
        $dir->save($data['name'], $data['content']);
    }
}

$result = "";

$ajax = new Ajax();

// call the provided action
$action = !empty($_POST['action']) ? $_POST['action'] : null;
if ($action && is_callable([$ajax, $action])) {
    unset($_POST['action']);
    $result = $ajax->$action($_POST);
}


return json_encode(['result' => $result]);
