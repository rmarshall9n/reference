<?php

/**
* Parser for converting reference files
*/
class RefParser
{

    function __construct() {}


    public function parse($html)
    {
        $html = nl2br($html);
        $html = preg_replace_callback_array(
            [
                // headings
                '/(-+)\s(.+)/' => function (&$matches) {
                    $h = strlen($matches[1]);
                    return '<h'.$h.'>'.trim(str_replace(['<br />', '<br>'], '', $matches[2])).'</h'.$h.'>';
                },

                // titles
                '/-T\s(.+)/i' => function (&$matches) {
                    return '<h1 class="title text-uppercase">'.trim(str_replace(['<br />', '<br>'], '', $matches[1])).'</h1>';
                },

                // code
                '/-C:?(?\'lang\'[\w]+)?(?\'code\'.*?)--C/s' => function (&$matches) {
                    $lang = isset($matches['lang']) ? 'class="'.$matches['lang'].'"': '';
                    $code = isset($matches['code']) ? $matches['code'] : '';
                    return "<pre><code {$lang}>".htmlspecialchars(str_replace(['<br />', '<br>'], '', $code)).'</code></pre>';
                },

                // multiple <br>
                '/(<br \/>\s*){2,}/' => function (&$matches) {
                    return "<br \>\n";
                },
            ],
            $html
        );

        return $html;
    }
}