<?php

define('DS', DIRECTORY_SEPARATOR);

/**
* Directory handler
*/
class DirectoryManager
{
    public $path;

    private $listing = [];

    function __construct($path = 'refdoc')
    {
        $this->path = $path;
    }

    public function getListing()
    {
        return $this->listing;
    }

    private function getPath($name = null)
    {
        return ($name == null) ? dirname(__DIR__).'/'.$this->path : dirname(__DIR__).'/'.$this->path.DS.$name.'.txt';
    }

    public function getDirectoryListing()
    {
        $this->listing = [];

        foreach (new DirectoryIterator($this->path) as $fileInfo) {
            if($fileInfo->isDot() || $fileInfo->getFilename() == 'default.txt') {
                continue;
            }
            $this->listing[substr($fileInfo->getFilename(), 0, -4)] = $this->path . DS . $fileInfo->getFilename();
        }

        asort($this->listing);
        return $this->listing;
    }

    public function exists($name)
    {
        if (empty($name)) {
            return false;
        }

        return file_exists($this->getPath($name));
    }

    public function getRefContent($name)
    {
        ob_start();
        include $this->getPath($name);
        return ob_get_clean();
    }


    public function save($name, $content)
    {
        if ($this->exists($name)) {
            file_put_contents($this->getPath($name), $content);
        }
    }

    public function create($name)
    {
        if ($this->exists($name)) {
            return false;
        }

        touch($this->getPath().DS.$name.'.txt');
        return true;
    }
}
