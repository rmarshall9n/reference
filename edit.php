<?php

require_once 'header.php';


if (isset($_POST['save'])) {
    $dir->save($refName, $_POST['content']);
    header('location:index.php?r='.$refName);
    exit;
}


$content = '';
if($dir->exists($refName)) {
    $content = $dir->getRefContent($refName);

}


?>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-md-2">
            <h1></h1>
            <ul id="thisMenu" class="nav nav-pills nav-stacked">
                <?php foreach ($dir->getListing() as $name => $path) : ?>
                    <li><a href="?r=<?= $name; ?>"><?= $name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-10">
            <form id="editForm" action="" method="post">
                <input type="hidden" name="r" value="<?= $name; ?>">
                <div class="form-group">
                    <textarea id="edit" name="content" class="form-control" rows="30"><?= $content; ?></textarea>
                </div>

                <button type="submit" name="save" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        var $edit = $('#edit');

        // key bindings on the text area
        $edit.bind('keydown', function(e) {
            if(e.ctrlKey && (e.which == 83)) {
                // save
                e.preventDefault();
                save('<?= $refName; ?>', $edit.val());
                return false;
            } else if(e.which == 9) {
                // tab
                e.preventDefault();
                insertTab($edit);
                return false;
            }
        });
    });

    function save(name, content) {

        $.post( "app/Ajax.php", {action: "save", content: content, name: name }, function() {
          alert( "Data Loaded: " + data );
        });
    }

    function insertTab($textarea) {

        var start = $textarea.get(0).selectionStart;
        var end = $textarea.get(0).selectionEnd;

        // set textarea value to: text before caret + tab + text after caret
        $textarea.val($textarea.val().substring(0, start)
                    + "\t"
                    + $textarea.val().substring(end));

        // put caret at right position again
        $textarea.get(0).selectionStart =
        $textarea.get(0).selectionEnd = start + 1;
    }

</script>

<?php require_once 'footer.php'; ?>